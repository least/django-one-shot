from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from .models import TodoItem, TodoList
from .forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            todo_list = get_object_or_404(
                TodoList, name=form.cleaned_data["name"]
            )
            return redirect(todo_list_detail, todo_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)
        context = {
            "todo_list": todo_list,
            "form": form,
        }
        return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_list = get_object_or_404(
                TodoList, name=form.cleaned_data["list"]
            )
            form.save()
            return redirect("todo_list_detail", todo_list.id)
    else:
        form = TodoItemForm()
        context = {"form": form}

    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_list = get_object_or_404(
                TodoList, name=form.cleaned_data["list"]
            )
            form.save()
            form.save()
        return redirect("todo_list_detail", todo_list.id)
    else:
        form = TodoItemForm(instance=todo_item)
        context = {
            "todo_item": todo_item,
            "form": form,
        }
    return render(request, "todos/items/update.html", context)
